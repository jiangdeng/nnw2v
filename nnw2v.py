import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def loadVocab(data_url):
    """
    :param data_url: the data_url to the data path
    :return:
    """

    vocabs = set()
    label_counts = dict()
    with open(data_url, 'r') as fp:
        lines = fp.readlines()
        for line in lines:
            buf = line.split()
            if(buf[0] not in label_counts):
                label_counts[buf[0]] = 0
            label_counts[buf[0]] += 1
            # mean_len += len(buf)
            for word in buf:
                vocabs.add(word)
        # mean_len /= len(lines)
        # print(label_counts)

    return label_counts, vocabs


def loadData(data_url, vector_dim, case_len, word_vectors=None):
    """
    Load data from specific url

    """

    labels, vocabs = loadVocab(data_url)

    if word_vectors is None:
        word_vectors = dict()

    # np.random.seed(1)
    for word in vocabs:
        if word not in word_vectors:
            word_vectors[word] = list(np.random.randn(vector_dim))

    with open(data_url, 'r') as fp:
        lines = fp.readlines()
    X = list()
    y = list()
    for line in lines:
        sp_line = line.split()
        y.append(1 if sp_line[0][0] == 'e' else 0)  # label = earn ? 1 : 0
        slen = len(sp_line)
        if slen > case_len:
            slen = case_len
        for i in range(1, slen):
            X.append(word_vectors[sp_line[i]])
        for i in range(slen, case_len + 1):
            X.append(list(np.zeros(vector_dim)))

    # print("len(x) = %d, len(y) = %d" % (len(X), len(y)))

    X = np.array(X)
    X = X.reshape(-1, vector_dim * case_len)
    X = X.T
    y = np.array(y)
    y = y.reshape(1, -1)

    return X, y


def train(X, y, parameters, learning_rate=0.1):
    """
    Pretreat the training data

    """
    W1, b1, W2, b2 = parameters

    # Forward propagation
    Z1 = W1.dot(X) + b1
    A1 = np.tanh(Z1)
    Z2 = W2.dot(A1) + b2
    A2 = sigmoid(Z2)
    predictions = A2 > 0.5

    # Compute cost
    m = y.shape[1]

    logprobs = y * np.log(A2) + (1 - y) * np.log((1 - A2))
    cost = -np.sum(logprobs) / m

    # Backward propagation
    dZ2 = A2 - y
    dW2 = dZ2.dot(A1.T) / m
    db2 = np.sum(dZ2, axis=1, keepdims=True) / m
    dZ1 = np.dot(W2.T, dZ2) * (1 - A1 ** 2)
    dW1 = dZ1.dot(X.T) / m
    db1 = np.sum(dZ1, axis=1, keepdims=True) / m
    dX = np.dot(W1.T, dZ1) / m

    # Update parameters
    W1 = W1 - learning_rate * dW1
    b1 = b1 - learning_rate * db1
    W2 = W2 - learning_rate * dW2
    b2 = b2 - learning_rate * db2
    X[X!=0] = X[X!=0] - learning_rate * dX[X!=0]

    parameters = W1, b1, W2, b2
    return X, parameters, cost, predictions


def initialize_parameters(n0, n1, n2):
    """

    :param n0: The size of the input layer
    :param n1: The size of the hidden layer
    :param n2: The size of the output layer
    :return:
    """

    W1 = np.random.randn(n1, n0) * 0.1
    b1 = np.zeros((n1, 1))
    W2 = np.random.randn(n2, n1) * 0.1
    b2 = np.zeros((n2, 1))

    return W1, b1, W2, b2


def model(X, y, hidden_layer_size, num_iterations):
    np.random.seed(1)
    n0 = X.shape[0]
    n1 = hidden_layer_size
    n2 = y.shape[0]
    parameters = initialize_parameters(n0, n1, n2)

    for i in range(num_iterations):
        X, parameters, cost, predictions = train(X, y, parameters, learning_rate=0.1)
        if i % 2 == 0:
            print("Cost after iteration #%d: %f" % (i, cost))
            accuracy = (np.dot(y, predictions.T) + np.dot(1 - y, 1 - predictions.T)) / y.size * 100
            print('Accuracy: %f' % accuracy + '%')


    return X, parameters


def predict(parameters, X):
    W1, b1, W2, b2 = parameters

    # Forward propagation
    Z1 = W1.dot(X) + b1
    A1 = np.tanh(Z1)
    Z2 = W2.dot(A1) + b2
    A2 = sigmoid(Z2)

    predictions = (A2 > 0.5)
    return predictions


def get_vectors(train_url, X, vector_dim, case_len):
    word_vectors = dict()

    X = X.reshape(-1, vector_dim)
    print(X.shape)
    X = X.tolist()
    cnt = 0

    with open(train_url, 'r') as fp:
        lines = fp.readlines()

    for line in lines:
        sp_line = line.split()
        slen = len(sp_line)
        if slen > case_len:
            slen = case_len
        for i in range(1, slen):
            word_vectors[sp_line[i]] = X[cnt]
            cnt += 1
        for i in range(slen, case_len + 1):
            cnt += 1

    return word_vectors


def main():
    """ Test the module
    """
    train_url = "dataset/r8-train-stemmed.txt"
    test_url = "dataset/r8-test-stemmed.txt"
    vector_dim = 100
    hidden_layer_size = 128
    num_iterations = 100
    case_len = 60

    X, y = loadData(train_url, vector_dim, case_len)
    print(X.T[0].shape, y.T[0])
    print("X.shape = %s , y.shape = %s" % (X.shape, y.shape))
    X, parameters = model(X, y, hidden_layer_size, num_iterations)

    word_vectors = get_vectors(train_url, X, vector_dim, case_len)

    testX, testy = loadData(test_url, vector_dim, case_len, word_vectors)

    predictions = predict(parameters, testX)
    accuracy = (np.dot(testy, predictions.T) + np.dot(1 - testy, 1 - predictions.T)) / testy.size * 100
    print('Accuracy: %s' % accuracy + '%')

    params = str(vector_dim) + "_" + str(hidden_layer_size) + "_" + str(num_iterations)
    with open("wordvectors" + params + ".txt", "w") as fp:
        fp.write('Accuracy: %d' % accuracy + '%\n')
        for word in word_vectors:
            fp.write(word + ":" + str(word_vectors[word]) + '\n')

    pass


if __name__ == '__main__':
    main()

# test 8577 8
# train 14577 10
# {'ship', 'interest', 'money-fx', 'acq', 'crude', 'earn', 'grain', 'trade'}
# minlen 5
# maxlen 534
# meanlen = 67
